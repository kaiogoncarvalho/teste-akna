<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ShoppingListsProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shopping_lists_products', function (Blueprint $table) {
            $table->integer('shopping_list_id')->unsigned();
            $table->integer('product_id')->unsigned();
            $table->primary(['shopping_list_id', 'product_id']);
            $table->integer('quantity');
            $table->foreign('shopping_list_id')->references('id')->on('shopping_lists');
            $table->foreign('product_id')->references('id')->on('products');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('shopping_lists_products');
        Schema::enableForeignKeyConstraints();
    }
}
