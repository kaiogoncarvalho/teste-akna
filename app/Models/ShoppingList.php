<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ShoppingList extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'id',
        'month',
        'year',
        'created_at',
        'updated_at'
    ];

    protected $dates = [
        'deleted_at',
        'created_at',
        'updated_at'
    ];

    protected $hidden = [
        'deleted_at'
    ];

    protected $casts = [
        'month' => 'date:m',
        'year'  => 'date:Y',
    ];

    public function products()
    {
        return $this->belongsToMany('App\Models\Product','shopping_lists_products');
    }

}
