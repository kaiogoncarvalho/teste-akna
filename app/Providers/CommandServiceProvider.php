<?php

namespace App\Providers;

use App\Console\Commands\ShoppingListCSV;
use Illuminate\Support\ServiceProvider;
use App\Console\Commands\ShoppingListDatabase;


class CommandServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register()
    {
        $this->registerCommand(
            'command.shopping-list.csv',
            ShoppingListCSV::class
        );

        $this->registerCommand(
            'command.shopping-list.database',
            ShoppingListDatabase::class
        );
    }

    private function registerCommand($id, $class)
    {
        $this->app->singleton($id, function () use ($class) {
            return app($class);
        });

        $this->commands($id);
    }
}
