<?php

namespace App\Console\Commands;

use App\Models\Category;
use App\Models\Product;
use App\Models\ShoppingList;

class ShoppingListDatabase extends AbstractShoppingList
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'shopping-list:database';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create CSV by php file';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->alert("Shopping List CSV initialized:");

        $callback = function ($array) {
            $month = $this->month->getNumberByName($array[0]);
            $categoryName = $array[1];
            $productName = $array[2];
            $quantity = $array[3];
            /**
             * @var Category $category
             */
            $category = Category::updateOrCreate(['name' => $categoryName]);

            /**
             * @var Product $product
             */
            $product = Product::updateOrCreate(
                [
                    'name' => $productName,
                    'category_id' => $category->getAttribute('id')
                ]
            );

            /**
             * @var ShoppingList $shoppingList
             */
            $shoppingList = ShoppingList::updateOrCreate(
                [
                    'month' => $month,
                    'year' => date('Y')
                ]
            );

            $shoppingList->products()->syncWithoutDetaching(
                [
                    $product->getAttribute('id') => ['quantity' => $quantity]
                ]
            );


        };

        $this->processLines($callback);

        $this->info("Shopping List in Database!");
    }
}
