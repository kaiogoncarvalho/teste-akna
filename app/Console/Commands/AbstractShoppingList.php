<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Console\Commands\Support\Month;
use League\Csv\Writer;
use App\Console\Commands\Support\SpellChecker;

abstract class AbstractShoppingList extends Command
{
    /**
     * @var Month
     */
    protected $month;

    /**
     * @var array
     */
    private $shoppingList;

    /**
     * @var SpellChecker
     */
    private $spellChecker;

    public function __construct(Month $month)
    {
        $this->month = $month;
        $this->spellChecker = SpellChecker::getInstance();
        $this->shoppingList = include(
            app_path('/Console/Commands/Seed/lista-de-compras.php')
        );
        parent::__construct();
    }

    public function correct($word): string
    {
        $words = $this->spellChecker->getWords($word);
        $newWord = [];

        foreach ($words as $oneWord)
        {
            $correctWord = $this->spellChecker->getUcFirst($oneWord);

            if($this->spellChecker->getSalvedWord($correctWord)){
                $newWord[] = $this->spellChecker->getSalvedWord($correctWord);
                continue;
            }

            if(!$this->spellChecker->isValidWord($correctWord)){
                $suggestions = $this->spellChecker->getSugestions($oneWord);
                $correctWord = $this->choice(
                    "A palavra '$oneWord' está incorreta em '$word', qual seria a correta?",
                    array_slice($suggestions, 0, 9) + [9 => 'Informar a Palavra'],
                    0);

                if( $correctWord === 'Informar a Palavra' ){
                    $correctWord = $this->ask('Qual seria a Palavra?');
                }

                $this->spellChecker->setSalvedWord($oneWord, $correctWord);
            }

            $newWord[] = $correctWord;

        }

        return implode(' ', $newWord);
    }

    public function processLines($callback)
    {
        $callbackOrder = function ($a, $b) {
            return $this->month->orderByNames($a, $b);
        };

        uksort($this->shoppingList, $callbackOrder);
        foreach ($this->shoppingList as $month => $types){
            ksort($types);
            foreach ($types as $type => $products){
                arsort($products);
                foreach ($products as $product => $quantity){
                    $callback(
                        [
                            $this->month->getCorrectNameOfMonth($month),
                            $this->correct($type),
                            $this->correct($product),
                            $quantity
                        ]
                    );
                }
            }
        }
    }


}
