<?php

namespace App\Console\Commands;

use League\Csv\Writer;

class ShoppingListCSV extends AbstractShoppingList
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'shopping-list:csv';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create CSV by php file';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->alert("Shopping List CSV initialized:");

        $header = ['Mês', 'Categoria', 'Produto', 'Quantidade'];
        $csvDir = storage_path('app/public/shopping-list-'.date('Y').'.csv');
        $csv = Writer::createFromPath($csvDir, 'w+');
        $csv->insertOne($header);

        $callback = function ($array) use ($csv){
            $csv->insertOne($array);
        };

        $this->processLines($callback);

        $this->info("Shopping List CSV finished!");

        $this->info("CSV is in {$csvDir}");

    }
}
