<?php


namespace App\Console\Commands\Support;


class SpellChecker
{
    /**
     * @var self
     */
    private static $instance;

    private $ignoreUpper = [
        'de',
        'da',
        'ao',
        'em'
    ];

    private $wordsIncluded = [
        'Nutella',
        'Multiuso',
        'Protex',
        'Dove',
        'Mignon',
        'MOP',
        'Doritos',
        'Leite'
    ];

    private static $wordsSalved = [];

    private $pspellLink;

    private function __construct()
    {
        $this->pspellLink = pspell_new("pt_BR", "", "", "utf-8");
        foreach ($this->wordsIncluded as $word){
            pspell_add_to_session($this->pspellLink, $word);
        }
    }

    /**
     * @return $this
     */
    public static function getInstance(): self
    {
        if (self::$instance == null) {
            self::$instance = new SpellChecker();
        }

        return self::$instance;
    }

    public function getWords(string $word): array
    {
        $delimiter = ' ';
        if(preg_match('/[_]+/', $word)){
            $delimiter = '_';
        }

        return explode($delimiter, $word);
    }

    public function getUcFirst($word)
    {
        if(!in_array($word, $this->ignoreUpper)){
            return ucfirst($word);
        }

        return $word;
    }

    /**
     * @param string $word
     * @return mixed|string
     */
    public function getSugestions(string $word): array
    {
        return pspell_suggest($this->pspellLink, $word);
    }

    /**
     * @param $word
     * @param $correctWord
     */
    public function setSalvedWord($word, $correctWord): void
    {
        self::$wordsSalved[$word] = $correctWord;
    }

    /**
     * @param $word
     * @return bool|mixed
     */
    public function getSalvedWord($word)
    {
        if(array_key_exists($word, self::$wordsSalved)){
            return self::$wordsSalved[$word];
        }

        return false;
    }

    public function isValidWord($word): bool
    {
        return (pspell_check($this->pspellLink, $word));
    }
}
