<?php

namespace App\Console\Commands\Support;

/**
 * Class Month
 * @package App\Console\Commands\Support
 */
class Month
{
    /**
     * @var array
     */
    private $months = [
        1 => 'Janeiro',
        2 => 'Fevereiro',
        3 => 'Março',
        4 => 'Abril',
        5 => 'Maio',
        6 => 'Junho',
        7 => 'Julho',
        8 => 'Agosto',
        9 => 'Setembro',
        10 => 'Outubro',
        11 => 'Novembro',
        12 => 'Dezembro'
    ];

    /**
     * @param $a
     * @param $b
     * @return int
     */
    public function orderByNames($a, $b)
    {
        return $this->getNumberByName($a) >= $this->getNumberByName($b);
    }

    /**
     * @param $month
     * @return int
     */
    private function getValidNumberByName($month): int
    {
        $selectedMonth = null;
        $highestPercent = 0;
        foreach (array_flip($this->months) as $name => $number){
            similar_text($name, $month, $percent);
            if($percent > $highestPercent){
                $highestPercent = $percent;
                $selectedMonth = $number;
            }
        }

        if($highestPercent < 70){
            throw new \Exception("Invalid Month Name: {$month}");
        }

        return $selectedMonth;
    }

    /**
     * @param $monthName
     * @return int
     */
    public function getNumberByName($monthName): int
    {
        $formatName = ucfirst($monthName);
        if(array_key_exists(ucfirst($monthName), $this->months)){
            return array_flip($this->months)[$formatName];
        }

        return $this->getValidNumberByName($formatName);
    }

    public function getCorrectNameOfMonth($month)
    {
        $key = $this->getNumberByName($month);
        return $this->months[$key];
    }

}
