<?php


namespace App\Helpers;


class SpellCheckerHelper
{
    protected static $wordsIncluded = [
        'Nuttela',
        'Doritos'
    ];

    public static function correct(string $word)
    {

        $pspell_link = pspell_new("pt_BR");
        pspell_add_to_session($pspell_link, 'Nuttela');

        if (!pspell_check($pspell_link, "Marco")) {
            $suggestions = pspell_suggest($pspell_link, "Marco");

            foreach ($suggestions as $suggestion) {
                echo "Possible spelling: $suggestion\n";
            }
        }

    }


}
