create table categories
(
    id         int unsigned auto_increment
        primary key,
    name       varchar(100) not null,
    created_at timestamp    null,
    updated_at timestamp    null,
    deleted_at timestamp    null
)
    collate = utf8mb4_unicode_ci;

create table migrations
(
    id        int unsigned auto_increment
        primary key,
    migration varchar(255) not null,
    batch     int          not null
)
    collate = utf8mb4_unicode_ci;

INSERT INTO akna.migrations (id, migration, batch) VALUES (1, '2020_01_09_035707_shopping_lists', 1);
INSERT INTO akna.migrations (id, migration, batch) VALUES (2, '2020_01_09_040319_categories', 1);
INSERT INTO akna.migrations (id, migration, batch) VALUES (3, '2020_01_09_040324_products', 1);
INSERT INTO akna.migrations (id, migration, batch) VALUES (4, '2020_01_09_041335_shopping_lists_products', 1);

create table products
(
    id          int unsigned auto_increment
        primary key,
    name        varchar(100) not null,
    category_id int unsigned not null,
    created_at  timestamp    null,
    updated_at  timestamp    null,
    deleted_at  timestamp    null,
    constraint products_category_id_foreign
        foreign key (category_id) references categories (id)
)
    collate = utf8mb4_unicode_ci;

create table shopping_lists
(
    id         int unsigned auto_increment
        primary key,
    month      int unsigned not null,
    year       int          not null,
    created_at timestamp    null,
    updated_at timestamp    null,
    deleted_at timestamp    null,
    constraint shopping_lists_month_year_unique
        unique (month, year)
)
    collate = utf8mb4_unicode_ci;

create table shopping_lists_products
(
    shopping_list_id int unsigned not null,
    product_id       int unsigned not null,
    quantity         int          not null,
    primary key (shopping_list_id, product_id),
    constraint shopping_lists_products_product_id_foreign
        foreign key (product_id) references products (id),
    constraint shopping_lists_products_shopping_list_id_foreign
        foreign key (shopping_list_id) references shopping_lists (id)
)
    collate = utf8mb4_unicode_ci;

